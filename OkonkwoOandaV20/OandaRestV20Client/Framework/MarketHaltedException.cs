﻿using System;

namespace OandaRestV20Client.Framework
{
   public class MarketHaltedException : Exception
   {
      public MarketHaltedException(string message) : base(message) { }
   }
}
