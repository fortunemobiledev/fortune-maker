﻿using System;

namespace OandaRestV20Client.Framework
{
   public class IsOptionalAttribute : Attribute
   {
      public override string ToString()
      {
         return "Is Optional";
      }
   } 
}
