﻿using OandaRestV20Client.TradeLibrary.DataTypes.Instrument;
using System.Collections.Generic;

namespace OandaRestV20Client.Framework
{
   public interface IHasPrices
   {
      PriceInformation priceInformation { get; set; }
   }

   public class PriceInformation
   {
      public Instrument instrument { get; set; }
      public List<string> priceProperties { get; set; }
   }
}
