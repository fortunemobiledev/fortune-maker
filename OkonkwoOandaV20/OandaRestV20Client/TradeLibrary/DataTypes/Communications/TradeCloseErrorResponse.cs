﻿using OandaRestV20Client.TradeLibrary.DataTypes.Transaction;

namespace OandaRestV20Client.TradeLibrary.DataTypes.Communications
{
   public class TradeCloseErrorResponse : ErrorResponse
   {
      public MarketOrderRejectTransaction orderRejectTransaction { get; set; }
   }
}
