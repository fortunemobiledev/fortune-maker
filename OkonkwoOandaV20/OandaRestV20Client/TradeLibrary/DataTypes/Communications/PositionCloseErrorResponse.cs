﻿using OandaRestV20Client.TradeLibrary.DataTypes.Transaction;

namespace OandaRestV20Client.TradeLibrary.DataTypes.Communications
{
   public class PositionCloseErrorResponse : ErrorResponse
   {
      public MarketOrderRejectTransaction longOrderRejectTransaction { get; set; }
      public MarketOrderRejectTransaction shortOrderRejectTransaction { get; set; }
   }
}
