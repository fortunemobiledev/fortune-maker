﻿namespace OandaRestV20Client.TradeLibrary.DataTypes.Communications
{
   public class PositionResponse : Response
   {
      public Position.Position position { get; set; }
   }
}
