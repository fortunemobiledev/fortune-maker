﻿using OandaRestV20Client.TradeLibrary.DataTypes.Transaction;

namespace OandaRestV20Client.TradeLibrary.DataTypes.Communications
{
   public class AccountConfigurationErrorResponse : ErrorResponse
   {
      public ClientConfigureRejectTransaction clientConfigureRejectTransaction { get; set; }
   }
}
