﻿using System.Collections.Generic;

namespace OandaRestV20Client.TradeLibrary.DataTypes.Communications
{
   public class AccountInstrumentsResponse : Response
   {
      public List<Instrument.Instrument> instruments { get; set; }
   }
}
