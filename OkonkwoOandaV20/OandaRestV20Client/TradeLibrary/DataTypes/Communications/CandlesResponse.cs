﻿using OandaRestV20Client.TradeLibrary.DataTypes.Instrument;
using System.Collections.Generic;

namespace OandaRestV20Client.TradeLibrary.DataTypes.Communications
{
   public class CandlesResponse : Response
   {
      public string instrument;
      public string granularity;
      public List<Candlestick> candles;
   }
}
