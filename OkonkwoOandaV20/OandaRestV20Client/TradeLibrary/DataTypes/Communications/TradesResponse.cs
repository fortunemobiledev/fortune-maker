﻿using System.Collections.Generic;

namespace OandaRestV20Client.TradeLibrary.DataTypes.Communications
{
   public class TradesResponse : Response
   {
      public List<Trade.Trade> trades { get; set; }
   }
}
