﻿namespace OandaRestV20Client.TradeLibrary.DataTypes.Communications
{
   public class AccountResponse : Response
   {
      public Account.Account account { get; set; }
   }
}
