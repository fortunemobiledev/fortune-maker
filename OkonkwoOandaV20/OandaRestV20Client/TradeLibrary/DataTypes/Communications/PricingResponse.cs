﻿using OandaRestV20Client.TradeLibrary.DataTypes.Pricing;
using System.Collections.Generic;

namespace OandaRestV20Client.TradeLibrary.DataTypes.Communications
{
   public class PricingResponse : Response
   {
      public List<Price> prices;
   }
}
