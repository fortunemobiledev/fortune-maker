﻿using System.Collections.Generic;

namespace OandaRestV20Client.TradeLibrary.DataTypes.Communications
{
   public class PositionsResponse : Response
   {
      public List<Position.Position> positions { get; set; }
   }
}
