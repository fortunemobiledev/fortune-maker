﻿using OandaRestV20Client.TradeLibrary.DataTypes.Account;

namespace OandaRestV20Client.TradeLibrary.DataTypes.Communications
{
   public class AccountSummaryResponse : Response
   {
      public AccountSummary account { get; set; }
   }
}
