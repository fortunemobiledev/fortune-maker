﻿using Newtonsoft.Json;
using OandaRestV20Client.Framework.JsonConverters;
using OandaRestV20Client.TradeLibrary.DataTypes.Order;

namespace OandaRestV20Client.TradeLibrary.DataTypes.Communications
{
   public class OrderResponse : Response
   {
      [JsonConverter(typeof(OrderConverter))]
      public IOrder order { get; set; }
   }
}
