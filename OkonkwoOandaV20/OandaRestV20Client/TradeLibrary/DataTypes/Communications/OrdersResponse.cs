﻿using Newtonsoft.Json;
using OandaRestV20Client.Framework.JsonConverters;
using OandaRestV20Client.TradeLibrary.DataTypes.Order;
using System.Collections.Generic;

namespace OandaRestV20Client.TradeLibrary.DataTypes.Communications
{
   public class OrdersResponse : Response
   {
      [JsonConverter(typeof(OrderConverter))]
      public List<IOrder> orders { get; set; }
   }
}
