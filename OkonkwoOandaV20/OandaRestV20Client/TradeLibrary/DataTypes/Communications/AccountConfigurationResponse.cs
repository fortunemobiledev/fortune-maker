﻿using OandaRestV20Client.TradeLibrary.DataTypes.Transaction;

namespace OandaRestV20Client.TradeLibrary.DataTypes.Communications
{
   public class AccountConfigurationResponse : Response
   {
      public ClientConfigureTransaction clientConfigureTransaction;
   }
}
