﻿using OandaRestV20Client.TradeLibrary.DataTypes.Transaction;

namespace OandaRestV20Client.TradeLibrary.DataTypes.Communications
{
   public class OrderClientExtensionsModifyResponse : Response
   {
      public OrderClientExtensionsModifyTransaction orderClientExtensionsModifyTransaction { get; set; }
   }
}
