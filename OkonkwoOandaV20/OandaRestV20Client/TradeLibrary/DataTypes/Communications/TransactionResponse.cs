﻿using Newtonsoft.Json;
using OandaRestV20Client.Framework.JsonConverters;
using OandaRestV20Client.TradeLibrary.DataTypes.Transaction;

namespace OandaRestV20Client.TradeLibrary.DataTypes.Communications
{
   /// <summary>
   /// http://developer.oanda.com/rest-live-v20/transaction-ep/
   /// </summary>
   public class TransactionResponse : Response
   {
      [JsonConverter(typeof(TransactionConverter))]
      public ITransaction transaction { get; set; }
   }
}
