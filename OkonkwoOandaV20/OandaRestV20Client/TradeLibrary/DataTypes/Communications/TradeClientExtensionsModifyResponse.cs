﻿using OandaRestV20Client.TradeLibrary.DataTypes.Transaction;

namespace OandaRestV20Client.TradeLibrary.DataTypes.Communications
{
   public class TradeClientExtensionsModifyResponse : Response
   {
      public TradeClientExtensionsModifyTransaction tradeClientExtensionsModifyTransaction { get; set; }
   }
}
