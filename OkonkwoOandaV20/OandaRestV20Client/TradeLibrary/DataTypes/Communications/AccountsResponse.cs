﻿using OandaRestV20Client.TradeLibrary.DataTypes.Account;
using System.Collections.Generic;

namespace OandaRestV20Client.TradeLibrary.DataTypes.Communications
{
   public class AccountsResponse : Response
	{
		public List<AccountProperties> accounts;
	}
}
