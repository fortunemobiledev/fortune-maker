﻿using OandaRestV20Client.TradeLibrary.DataTypes.Transaction;

namespace OandaRestV20Client.TradeLibrary.DataTypes.Communications.Requests
{
   public class PatchExitOrdersRequest : Request
   {
      public TakeProfitDetails takeProfit { get; set; }
      public StopLossDetails stopLoss { get; set; }
      public TrailingStopLossDetails trailingStopLoss { get; set; }
   }
}
