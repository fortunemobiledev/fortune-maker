﻿using OandaRestV20Client.TradeLibrary.DataTypes.Transaction;

namespace OandaRestV20Client.TradeLibrary.DataTypes.Communications.Requests.Order
{
   public interface IOrderRequest
   {
      string type { get; set; }
      ClientExtensions clientExtensions { get; set; }
   }
}
