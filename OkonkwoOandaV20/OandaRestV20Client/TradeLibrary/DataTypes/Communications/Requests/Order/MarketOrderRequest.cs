﻿using OandaRestV20Client.TradeLibrary.DataTypes.Order;

namespace OandaRestV20Client.TradeLibrary.DataTypes.Communications.Requests.Order
{
   public class MarketOrderRequest : EntryOrderRequest
   {
      public MarketOrderRequest(Instrument.Instrument oandaInstrument)
         : base(oandaInstrument)
      {
         type = OrderType.Market;
         timeInForce = TimeInForce.FillOrKill;
      }
   }
}
