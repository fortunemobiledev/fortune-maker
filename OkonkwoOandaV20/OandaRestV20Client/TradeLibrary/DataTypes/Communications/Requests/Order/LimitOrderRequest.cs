﻿using OandaRestV20Client.TradeLibrary.DataTypes.Order;

namespace OandaRestV20Client.TradeLibrary.DataTypes.Communications.Requests.Order
{
   public class LimitOrderRequest : EntryOrderRequest
   {
      public LimitOrderRequest(Instrument.Instrument instrument) : base(instrument)
      {
         type = OrderType.Limit;
         timeInForce = TimeInForce.GoodUntilCancelled;
         triggerCondition = OrderTriggerCondition.Default;
      }

      public string gtdTime { get; set; }
      public string triggerCondition { get; set; }
   }
}
