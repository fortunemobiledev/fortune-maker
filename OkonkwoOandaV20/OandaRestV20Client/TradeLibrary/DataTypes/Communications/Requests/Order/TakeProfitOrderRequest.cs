﻿using OandaRestV20Client.TradeLibrary.DataTypes.Order;

namespace OandaRestV20Client.TradeLibrary.DataTypes.Communications.Requests.Order
{
   public class TakeProfitOrderRequest : ExitOrderRequest
   {
      public TakeProfitOrderRequest(Instrument.Instrument oandaInstrument)
         : base(oandaInstrument)
      {
         type = OrderType.TakeProfit;
      }
   }
}
