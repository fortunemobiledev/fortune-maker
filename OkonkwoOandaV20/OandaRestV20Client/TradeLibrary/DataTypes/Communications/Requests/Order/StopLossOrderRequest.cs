﻿using OandaRestV20Client.TradeLibrary.DataTypes.Order;

namespace OandaRestV20Client.TradeLibrary.DataTypes.Communications.Requests.Order
{
   public class StopLossOrderRequest : ExitOrderRequest
   {
      public StopLossOrderRequest(Instrument.Instrument oandaInstrument)
         : base(oandaInstrument)
      {
         type = OrderType.StopLoss;
      }
   }
}
