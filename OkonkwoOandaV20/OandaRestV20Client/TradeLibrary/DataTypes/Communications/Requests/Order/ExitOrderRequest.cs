﻿using Newtonsoft.Json;
using OandaRestV20Client.Framework;
using OandaRestV20Client.Framework.JsonConverters;
using OandaRestV20Client.TradeLibrary.DataTypes.Transaction;
using System.Collections.Generic;

namespace OandaRestV20Client.TradeLibrary.DataTypes.Communications.Requests.Order
{
   [JsonConverter(typeof(PriceObjectConverter))]
   public abstract class ExitOrderRequest : Request, IOrderRequest, IHasPrices
   {
      public ExitOrderRequest(Instrument.Instrument oandaInstrument)
      {
         priceInformation = new PriceInformation()
         {
            instrument = oandaInstrument,
            priceProperties = new List<string>() { "price" }
         };
      }

      public string type { get; set; }
      public decimal? price { get; set; }
      public long tradeID { get; set; }
      public string clientTradeID { get; set; }
      public string timeInForce { get; set; }
      public string gtdTime { get; set; }
      public string triggerCondition { get; set; }
      public ClientExtensions clientExtensions { get; set; }
      [JsonIgnore]
      public PriceInformation priceInformation { get ; set; }
   }
}
