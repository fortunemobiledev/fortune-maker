﻿using OandaRestV20Client.TradeLibrary.DataTypes.Account;

namespace OandaRestV20Client.TradeLibrary.DataTypes.Communications
{
   public class AccountChangesResponse : Response
   {
      public AccountChanges changes { get; set; }
      public AccountChangesState state { get; set; }
   }
}
