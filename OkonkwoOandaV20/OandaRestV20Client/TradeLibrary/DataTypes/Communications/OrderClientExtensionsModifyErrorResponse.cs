﻿using OandaRestV20Client.TradeLibrary.DataTypes.Transaction;
using System.Collections.Generic;

namespace OandaRestV20Client.TradeLibrary.DataTypes.Communications
{
   public class OrderClientExtensionsModifyErrorResponse : ErrorResponse
   {
      public OrderClientExtensionsModifyRejectTransaction orderClientExtensionsModifyRejectTransaction { get; set; }
   }
}
