﻿namespace OandaRestV20Client.TradeLibrary.DataTypes.Communications
{
   public class TradeResponse : Response
   {
      public Trade.Trade trade { get; set; }
   }
}
