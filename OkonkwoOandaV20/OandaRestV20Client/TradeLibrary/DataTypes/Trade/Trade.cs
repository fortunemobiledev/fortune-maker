﻿using OandaRestV20Client.TradeLibrary.DataTypes.Order;

namespace OandaRestV20Client.TradeLibrary.DataTypes.Trade
{
   public class Trade : TradeBase
   {
      public TakeProfitOrder takeProfitOrder { get; set; }
      public StopLossOrder stopLossOrder { get; set; }
      public TrailingStopLossOrder trailingStopLossOrder { get; set; }
   }
}
