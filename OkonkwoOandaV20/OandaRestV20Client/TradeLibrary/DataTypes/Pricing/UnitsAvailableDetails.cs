﻿namespace OandaRestV20Client.TradeLibrary.DataTypes.Pricing
{
   public class UnitsAvailableDetails
   {
      public long @long { get; set; }
      public long @short { get; set; }
   }
}