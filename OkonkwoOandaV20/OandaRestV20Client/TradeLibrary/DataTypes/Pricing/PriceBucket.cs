﻿namespace OandaRestV20Client.TradeLibrary.DataTypes.Pricing
{
   public class PriceBucket
   {
      public decimal price { get; set; }
      public int liquidity { get; set; }
   }
}