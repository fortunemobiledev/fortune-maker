﻿namespace OandaRestV20Client.TradeLibrary.DataTypes.Pricing
{
   public class PriceStatus
   {
      public const string Tradeable = "tradeable";
      public const string NonTradeable = "non-tradeable";
      public const string Invalid = "invalid";
   }
}
