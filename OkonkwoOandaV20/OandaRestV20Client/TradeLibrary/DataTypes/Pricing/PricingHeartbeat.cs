﻿namespace OandaRestV20Client.TradeLibrary.DataTypes.Pricing
{
   public class PricingHeartbeat
   {
      public string time { get; set; }
      public string type { get; set; }
   }
}
