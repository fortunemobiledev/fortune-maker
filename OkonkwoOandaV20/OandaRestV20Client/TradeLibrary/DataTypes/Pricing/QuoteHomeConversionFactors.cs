﻿namespace OandaRestV20Client.TradeLibrary.DataTypes.Pricing
{
   public class QuoteHomeConversionFactors
   {
      public decimal positiveUnits { get; set; }
      public decimal negativeUnits { get; set; }
   }
}