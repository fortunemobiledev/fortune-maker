﻿namespace OandaRestV20Client.TradeLibrary.DataTypes.Instrument
{
   public class CandleStickData
   {
      public decimal o { get; set; }
      public decimal h { get; set; }
      public decimal l { get; set; }
      public decimal c { get; set; }
   }
}
