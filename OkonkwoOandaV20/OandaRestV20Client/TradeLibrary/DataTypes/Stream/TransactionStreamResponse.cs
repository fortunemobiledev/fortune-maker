﻿using Newtonsoft.Json;
using OandaRestV20Client.Framework.JsonConverters;
using OandaRestV20Client.TradeLibrary.DataTypes.Transaction;

namespace OandaRestV20Client.TradeLibrary.DataTypes.Stream
{
   /// <summary>
   /// Events are authorized transactions posted to the subject account.
   /// For more information, visit: http://developer.oanda.com/rest-live-v20/transaction-ep/
   /// </summary>
   [JsonConverter(typeof(TransactionStreamResponseConverter))]
   public class TransactionStreamResponse : IHeartbeat
   {
      public TransactionHeartbeat heartbeat { get; set; }
      public ITransaction transaction { get; set; }

      public bool IsHeartbeat()
      {
         return (heartbeat != null);
      }
   }
}
