﻿namespace OandaRestV20Client.TradeLibrary.DataTypes.Stream
{
   public interface IHeartbeat
   {
      bool IsHeartbeat();
   }
}
