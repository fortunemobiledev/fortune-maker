﻿namespace OandaRestV20Client.TradeLibrary.DataTypes.Transaction
{
   public class ClientConfigureTransaction : Transaction
   {
      public string alias { get; set; }
      public decimal marginRate { get; set; }
   }
}