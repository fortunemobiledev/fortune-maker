﻿namespace OandaRestV20Client.TradeLibrary.DataTypes.Transaction
{
   public class ClientExtensions
   {
      public string id { get; set; }
      public string tag { get; set; }
      public string comment { get; set; }
   }
}
