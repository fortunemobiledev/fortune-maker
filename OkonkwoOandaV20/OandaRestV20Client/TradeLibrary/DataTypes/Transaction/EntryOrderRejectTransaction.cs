﻿namespace OandaRestV20Client.TradeLibrary.DataTypes.Transaction
{
   public abstract class EntryOrderRejectTransaction : EntryOrderTransaction
   {
      public string rejectReason { get; set; }
   }
}
