﻿namespace OandaRestV20Client.TradeLibrary.DataTypes.Transaction
{
   public class StopLossOrderRejectTransaction : ExitOrderRejectTransaction
   {
      public decimal price { get; set; }
   }
}
 