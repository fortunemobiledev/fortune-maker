﻿namespace OandaRestV20Client.TradeLibrary.DataTypes.Transaction
{
   public class StopLossOrderTransaction : ExitOrderTransaction
   {
      public decimal price { get; set; }
   }
}
 