﻿namespace OandaRestV20Client.TradeLibrary.DataTypes.Transaction
{
   public class TrailingStopLossOrderRejectTransaction : ExitOrderRejectTransaction
   {
      public decimal distance { get; set; }
   }
}
 