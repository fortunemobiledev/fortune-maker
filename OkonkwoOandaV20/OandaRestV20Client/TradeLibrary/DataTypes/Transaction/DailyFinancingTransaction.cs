﻿using System.Collections.Generic;

namespace OandaRestV20Client.TradeLibrary.DataTypes.Transaction
{
   public class DailyFinancingTransaction : Transaction
   {
      public decimal financing { get; set; }
      public decimal accountBalance { get; set; }
      public string accountFinancingMode { get; set; }
      public PositionFinancing positionFinancing { get; set; }
   }
}
