﻿namespace OandaRestV20Client.TradeLibrary.DataTypes.Transaction
{
   public class TakeProfitOrderTransaction : ExitOrderTransaction
   {
      public decimal price { get; set; }
   }
}
 