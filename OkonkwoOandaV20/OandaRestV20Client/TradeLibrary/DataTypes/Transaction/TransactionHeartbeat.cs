﻿namespace OandaRestV20Client.TradeLibrary.DataTypes.Transaction
{
   public class TransactionHeartbeat
   {
      public string type { get; set; }
      public long? lastTransactionID { get; set; }
      public string time { get; set; }
   }
}
