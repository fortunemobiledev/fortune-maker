﻿namespace OandaRestV20Client.TradeLibrary.DataTypes.Transaction
{
   public class TakeProfitOrderRejectTransaction : ExitOrderRejectTransaction
   {
      public decimal price { get; set; }
   }
}
 