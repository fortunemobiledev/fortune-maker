﻿namespace OandaRestV20Client.TradeLibrary.DataTypes.Transaction
{
   public class TrailingStopLossOrderTransaction : ExitOrderTransaction
   {
      public decimal distance { get; set; }
   }
}
 