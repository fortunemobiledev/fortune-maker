﻿using OandaRestV20Client.TradeLibrary.DataTypes.Transaction;
using System.Collections.Generic;

namespace OandaRestV20Client.TradeLibrary.DataTypes.Order
{
   public class MarketOrder : EntryOrder
   {
      public decimal? priceBound { get; set; }
      public MarketOrderTradeClose tradeClose { get; set; }
      public MarketOrderPositionCloseout longPositionCloseout { get; set; }
      public MarketOrderPositionCloseout shortPositionCloseout { get; set; }
      public MarketOrderMarginCloseout marginCloseout { get; set; }
      public MarketOrderDelayedTradeClose delayedTradeClose { get; set; }
   }
}
