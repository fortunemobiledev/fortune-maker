﻿namespace OandaRestV20Client.TradeLibrary.DataTypes.Order
{
   public class TrailingStopLossOrder : ExitOrder
   {
      public decimal distance { get; set; }
      public decimal trailingStopValue { get; set; }
   }
}
