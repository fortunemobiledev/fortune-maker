﻿using Newtonsoft.Json;
using OandaRestV20Client.Framework.JsonConverters;
using OandaRestV20Client.TradeLibrary.DataTypes.Order;
using OandaRestV20Client.TradeLibrary.DataTypes.Trade;
using OandaRestV20Client.TradeLibrary.DataTypes.Transaction;
using System.Collections.Generic;

namespace OandaRestV20Client.TradeLibrary.DataTypes.Account
{
   public class AccountChanges
   {
      [JsonConverter(typeof(OrderConverter))]
      public List<IOrder> ordersCreated { get; set; }
      [JsonConverter(typeof(OrderConverter))]
      public List<IOrder> ordersCancelled { get; set; }
      [JsonConverter(typeof(OrderConverter))]
      public List<IOrder> ordersFilled { get; set; }
      [JsonConverter(typeof(OrderConverter))]
      public List<IOrder> ordersTriggered { get; set; }

      public List<TradeSummary> tradesOpened { get; set; }
      public List<TradeSummary> tradesReduced { get; set; }
      public List<TradeSummary> tradesClosed { get; set; }
      public List<Position.Position> positions { get; set; }

      [JsonConverter(typeof(TransactionConverter))]
      public List<ITransaction> transactions { get; set; }
   }
}
