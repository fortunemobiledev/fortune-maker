﻿using System;

namespace FortuneMaker
{
    public static class Utils
    {
        public static string GetUnixTimeStamp(DateTime dt)
        {
            return ((int)(dt.Subtract(new DateTime(1970, 1, 1))).TotalSeconds).ToString();
        }
    }
}
