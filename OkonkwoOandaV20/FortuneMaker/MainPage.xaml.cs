﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using Microcharts;
using OandaRestV20Client;
using OandaRestV20Client.TradeLibrary.DataTypes.Instrument;
using OandaRestV20Client.TradeLibrary.DataTypes.Order;
using OandaRestV20Client.TradeLibrary.DataTypes.Trade;
using OandaRestV20Client.TradeLibrary.Primitives;
using SkiaSharp;
using Xamarin.Forms;

namespace FortuneMaker
{
    // Learn more about making custom code visible in the Xamarin.Forms previewer
    // by visiting https://aka.ms/xamarinforms-previewer
    [DesignTimeVisible(false)]
    public partial class MainPage : ContentPage
    {
        const string INSTRUMENT = InstrumentName.Currency.EURUSD;
        const string ACCOUNT_ID = "101-004-12997455-001";

        List<Trade> _openTrades = new List<Trade>();
        List<Trade> _closedTrades = new List<Trade>();

        public MainPage()
        {
            InitializeComponent();

            SetApiCredentials();

            datePicker.Date = DateTime.Today.AddDays(-1);
        }

        async void OnGoClicked(object sender, EventArgs ea)
        {
            await GetHistory();
        }

        void SetApiCredentials()
        {
            Console.WriteLine("Setting your V20 credentials ...");

            var environment = EEnvironment.Practice;
            var token = "0a917528ea7fedcd7e868818c5ba010d-93664aacd117dd1372cafbfb036a2eba";

            Credentials.SetCredentials(environment, token, ACCOUNT_ID);

            Console.WriteLine("Nice! Credentials are set.");
        }

        private async Task GetHistory()
        {
            lblProfit.Text = "Requesting data from server...";

            _openTrades.Clear();
            _closedTrades.Clear();

            DateTime dtStart = datePicker.Date.AddHours(9);
            DateTime dtFromLastDay = dtStart.AddDays(-1);
            DateTime dtFromLastWeek = dtStart.AddDays(-7);
            DateTime dtFromLastMonth = dtStart.AddDays(-30);
            DateTime dtFromLastYear = dtStart.AddDays(-365);

            var pars = new Dictionary<string, string>();
            pars.Add("granularity", CandleStickGranularity.Minutes30);
            pars.Add("price", "MAB");
            pars.Add("from", Utils.GetUnixTimeStamp(dtFromLastDay));
            pars.Add("to", Utils.GetUnixTimeStamp(dtStart));
            var candlesLastDay = await Rest20.GetCandlesAsync(INSTRUMENT, pars);

            var pars2 = new Dictionary<string, string>();
            pars2.Add("granularity", CandleStickGranularity.Hours04);
            pars2.Add("price", "M");
            pars2.Add("from", Utils.GetUnixTimeStamp(dtFromLastWeek));
            pars2.Add("to", Utils.GetUnixTimeStamp(dtFromLastDay));
            var candlesLastWeek = await Rest20.GetCandlesAsync(INSTRUMENT, pars2);

            var pars3 = new Dictionary<string, string>();
            pars3.Add("granularity", CandleStickGranularity.Daily);
            pars3.Add("price", "M");
            pars3.Add("from", Utils.GetUnixTimeStamp(dtFromLastMonth));
            pars3.Add("to", Utils.GetUnixTimeStamp(dtFromLastWeek));
            var candlesLastMonth = await Rest20.GetCandlesAsync(INSTRUMENT, pars3);

            var pars4 = new Dictionary<string, string>();
            pars4.Add("granularity", CandleStickGranularity.Weekly);
            pars4.Add("price", "M");
            pars4.Add("from", Utils.GetUnixTimeStamp(dtFromLastYear));
            pars4.Add("to", Utils.GetUnixTimeStamp(dtFromLastMonth));
            var candlesLastYear = await Rest20.GetCandlesAsync(INSTRUMENT, pars4);

            // Request the with most granularity from start date for a 1 day period
            var dtEnd = dtStart.AddDays(1);

            var dtTo = dtStart.AddHours(4);
            List<CandlestickPlus> candlesNextDay = new List<CandlestickPlus>();
            while (dtTo < dtEnd)
            {
                var pars5 = new Dictionary<string, string>();
                pars5.Add("granularity", CandleStickGranularity.Seconds05);
                pars5.Add("price", "MAB");
                pars5.Add("from", Utils.GetUnixTimeStamp(dtStart));
                pars5.Add("to", Utils.GetUnixTimeStamp(dtTo));
                candlesNextDay.AddRange(await Rest20.GetCandlesAsync(INSTRUMENT, pars5));

                dtStart = dtTo;
                dtTo = dtStart.AddHours(4);
            }

            DateTime dtCurrentCandleStart = DateTime.Parse(candlesNextDay[0].time);

            CandlestickPlus currentCandle = new CandlestickPlus
            {
                ask = new CandleStickData
                {
                    o = candlesNextDay[0].ask.o,
                    h = candlesNextDay[0].ask.h,
                    l = candlesNextDay[0].ask.l,
                    c = candlesNextDay[0].ask.c
                },
                mid = new CandleStickData
                {
                    o = candlesNextDay[0].mid.o,
                    h = candlesNextDay[0].mid.h,
                    l = candlesNextDay[0].mid.l,
                    c = candlesNextDay[0].mid.c
                },
                bid = new CandleStickData
                {
                    o = candlesNextDay[0].bid.o,
                    h = candlesNextDay[0].bid.h,
                    l = candlesNextDay[0].bid.l,
                    c = candlesNextDay[0].bid.c
                }
            };

            lblProfit.Text = "Processing data...";

            foreach (var candle in candlesNextDay)
            {
                var curTime = DateTime.Parse(candle.time);
                if ((curTime - dtCurrentCandleStart).TotalMinutes >= 30)
                {
                    currentCandle.time = dtCurrentCandleStart.ToString();   
                    candlesLastDay.Add(currentCandle);

                    dtCurrentCandleStart = curTime;

                    currentCandle = new CandlestickPlus { ask = new CandleStickData(), mid = new CandleStickData(), bid = new CandleStickData()};

                    currentCandle.ask.o = candle.ask.o;
                    currentCandle.mid.o = candle.mid.o;
                    currentCandle.mid.o = candle.bid.o;
                }

                if (candle.ask.h > currentCandle.ask.h) currentCandle.ask.h = candle.ask.h;
                if (candle.mid.h > currentCandle.mid.h) currentCandle.mid.h = candle.mid.h;
                if (candle.bid.h > currentCandle.bid.h) currentCandle.bid.h = candle.bid.h;
                if (candle.ask.l < currentCandle.ask.l || currentCandle.ask.l == default(decimal)) currentCandle.ask.l = candle.ask.l;
                if (candle.mid.l < currentCandle.mid.l || currentCandle.mid.l == default(decimal)) currentCandle.mid.l = candle.mid.l;
                if (candle.bid.l < currentCandle.bid.l || currentCandle.bid.l == default(decimal)) currentCandle.bid.l = candle.bid.l;

                currentCandle.ask.c = candle.ask.c;
                currentCandle.mid.c = candle.mid.c;
                currentCandle.bid.c = candle.bid.c;

                // Make the decision
                if (_openTrades.Count == 0)
                {
                    MakeBuySellDecision(candlesLastYear, candlesLastMonth, candlesLastWeek, candlesLastDay, currentCandle, candle.time);
                }
                else
                {
                    MakeCloseDecision(_openTrades[0], currentCandle, candle.time);
                }
            }

            // Lets see how much profit
            Console.WriteLine("** FINAL OUTCOME **");
            decimal sumProfit = 0;
            for (int i = 0; i < _closedTrades.Count; i++)
            {
                sumProfit += _closedTrades[i].realizedPL;

                string tradeType = String.Empty;

                if (_closedTrades[i].initialUnits > 0)
                    tradeType = "BUY";
                else
                    tradeType = "SELL";

                Console.WriteLine($"{tradeType} Trade {i}: { _closedTrades[i].realizedPL }");
            }

            Console.WriteLine($"Trades total: { sumProfit }");
            Console.WriteLine($"** {_openTrades.Count} Open trades **");

            lblProfit.Text = $"Your profit was {sumProfit}";

            float min = float.MaxValue;
            float max = float.MinValue;
            for (int i = 0; i < candlesNextDay.Count; i++)
            {
                var candle = candlesNextDay[i];

                if (min > (float)candle.mid.c)
                    min = (float)candle.mid.c;

                if (max < (float)candle.mid.c)
                    max = (float)candle.mid.c;
            }

            var entries = candlesNextDay?.Select(x => new Microcharts.Entry((float)x.mid.c)
            {
                Color = GetColor(x.time),
                TextColor = SKColor.Parse("000000")
            }).ToList() ?? null;

            chartView.Chart = new LineChart()
            {
                Entries = entries,
                LineMode = LineMode.Straight,
                PointMode = PointMode.Circle,
                LineSize = 1,
                BackgroundColor = SKColors.Transparent,
                MinValue = min,
                MaxValue = max,
                PointSize = 3
            };
        }

        SKColor GetColor(string time)
        {
            foreach (var trade in _closedTrades)
            {
                if (trade.openTime == time)
                {
                    return trade.initialUnits > 0 ? SKColor.Parse("#0000FF") : SKColor.Parse("#FFFF00");
                }
                else if (trade.closeTime == time)
                {
                    return trade.realizedPL > 0 ? SKColor.Parse("#00FF00") : SKColor.Parse("#FF0000");
                }
            }

            return SKColor.Parse("#000000");
        }

        void MakeBuySellDecision(List<CandlestickPlus> candlesLastYear, List<CandlestickPlus> candlesLastMonth, List<CandlestickPlus> candlesLastWeek, List<CandlestickPlus> candlesLastDay, CandlestickPlus currentCandle, string curTime)
        {
            // Get Year max, min and average
            decimal yearMax = default(decimal);
            decimal yearMin = default(decimal);
            decimal yearAvg = default(decimal);
            foreach (var candle in candlesLastYear)
            {
                if (candle.mid.h > yearMax) yearMax = candle.mid.h;
                if (candle.mid.l < yearMin || yearMin == default(decimal)) yearMin = candle.mid.l;

                yearAvg += candle.mid.c;
            }
            yearAvg /= candlesLastYear.Count;

            // Get Month max, min and average
            decimal monthMax = default(decimal);
            decimal monthMin = default(decimal);
            decimal monthAvg = default(decimal);
            foreach (var candle in candlesLastMonth)
            {
                if (candle.mid.h > monthMax) monthMax = candle.mid.h;
                if (candle.mid.l < monthMin || monthMin == default(decimal)) monthMin = candle.mid.l;

                monthAvg += candle.mid.c;
            }
            monthAvg /= candlesLastMonth.Count;

            // Get Week max, min and average
            decimal weekMax = default(decimal);
            decimal weekMin = default(decimal);
            decimal weekAvg = default(decimal);
            foreach (var candle in candlesLastWeek)
            {
                if (candle.mid.h > weekMax) weekMax = candle.mid.h;
                if (candle.mid.l < weekMin || weekMin == default(decimal)) weekMin = candle.mid.l;

                weekAvg += candle.mid.c;
            }
            weekAvg /= candlesLastWeek.Count;

            // Get Day max, min and average
            decimal dayMax = default(decimal);
            decimal dayMin = default(decimal);
            decimal dayAvg = default(decimal);
            foreach (var candle in candlesLastDay)
            {
                if (candle.mid.h > dayMax) dayMax = candle.mid.h;
                if (candle.mid.l < dayMin || dayMin == default(decimal)) dayMin = candle.mid.l;

                dayAvg += candle.mid.c;
            }
            dayAvg /= candlesLastDay.Count;

            // Check if last day candles are descending or ascending significally
            bool ascending = false;
            bool descending = false;
            decimal minVariation = default(decimal);
            for (int i = candlesLastDay.Count - 4; i < candlesLastDay.Count; i++)
            {
                if (candlesLastDay[i].mid.c < candlesLastDay[i - 1].mid.c && candlesLastDay[i].mid.h < candlesLastDay[i - 1].mid.h && candlesLastDay[i].mid.l < candlesLastDay[i - 1].mid.l)
                    descending = true;

                if (candlesLastDay[i].mid.c > candlesLastDay[i - 1].mid.c && candlesLastDay[i].mid.h > candlesLastDay[i - 1].mid.h && candlesLastDay[i].mid.l > candlesLastDay[i - 1].mid.l)
                    ascending = true;

                if (minVariation == default(decimal) || minVariation > Math.Abs(candlesLastDay[i].mid.l - candlesLastDay[i].mid.h))
                    minVariation = Math.Abs(candlesLastDay[i].mid.l - candlesLastDay[i].mid.h);
            }

            // If ascending, check if it's good to sell
            if (ascending && !descending)
            {
                // Check we're above year min, month avg, week avg and day avg
                if(currentCandle.mid.c > yearMin && currentCandle.mid.c > monthAvg && currentCandle.mid.c > weekAvg && currentCandle.mid.c > dayAvg)
                {
                    // Check if breaking the ascent
                    if(currentCandle.mid.h < candlesLastDay.Last().mid.h && currentCandle.mid.c < candlesLastDay.Last().mid.l)
                    {
                        // Check if spread is coverable by high and low difference
                        var spread = Math.Abs(currentCandle.ask.c - currentCandle.bid.c);

                        if (spread < minVariation * (decimal)0.8)
                        {
                            // Put on a SELL trade
                            var trade = new Trade();
                            trade.openTime = curTime;
                            trade.price = currentCandle.ask.c;
                            trade.initialUnits = -1000;
                            trade.takeProfitOrder = new TakeProfitOrder { price = candlesLastDay[candlesLastDay.Count - 3].bid.l };
                            trade.stopLossOrder = new StopLossOrder {  price = candlesLastDay[candlesLastDay.Count - 1].bid.h };
                            _openTrades.Add(trade);
                        }
                    }
                }
            }
            // If descending, check if it's good to buy
            else if (descending && !ascending)
            {
                // Check we're bellow year max, month avg, week avg and day avg
                if (currentCandle.mid.c < yearMax && currentCandle.mid.c < monthAvg && currentCandle.mid.c < weekAvg && currentCandle.mid.c < dayAvg)
                {
                    // Check if breaking the ascent
                    if (currentCandle.mid.l > candlesLastDay.Last().mid.l && currentCandle.mid.c > candlesLastDay.Last().mid.h)
                    {
                        // Check if spread is coverable by high and low difference
                        var spread = Math.Abs(currentCandle.ask.c - currentCandle.bid.c);

                        if (spread < minVariation * (decimal)0.8)
                        {
                            // Put on a BUY trade
                            var trade = new Trade();
                            trade.openTime = curTime;
                            trade.price = currentCandle.bid.c;
                            trade.initialUnits = 1000; // Buy
                            trade.takeProfitOrder = new TakeProfitOrder { price = candlesLastDay[candlesLastDay.Count - 3].ask.h };
                            trade.stopLossOrder = new StopLossOrder { price = candlesLastDay[candlesLastDay.Count - 1].ask.l };
                            _openTrades.Add(trade);
                        }
                    }
                }
            }
        }

        void MakeCloseDecision(Trade trade, CandlestickPlus currentCandle, string time)
        {
            // Buy trade
            if (trade.initialUnits > 0)
            {
                // Stop loss condition
                if (currentCandle.bid.c <= trade.stopLossOrder.price)
                {
                    trade.realizedPL = (currentCandle.bid.c - trade.price) * trade.initialUnits;
                    trade.closeTime = time;
                    _openTrades.Remove(trade);
                    _closedTrades.Add(trade);
                }

                // Take profit condition
                if (currentCandle.bid.c >= trade.takeProfitOrder.price)
                {
                    trade.realizedPL = (currentCandle.bid.c - trade.price) * trade.initialUnits;
                    trade.closeTime = time;
                    _openTrades.Remove(trade);
                    _closedTrades.Add(trade);
                }

                // Adjust SL
                if (currentCandle.bid.c > trade.price)
                {
                    // Set a positive SL if is yet negative
                    if (trade.stopLossOrder.price < trade.price)
                    {
                        trade.stopLossOrder.price = currentCandle.bid.c;
                    }

                    var adjustmentMargin = Math.Abs(trade.takeProfitOrder.price - trade.price) * (decimal)0.1;

                    // Grow SL to ensure more profit
                    if (trade.stopLossOrder.price < currentCandle.bid.c - adjustmentMargin)
                    {
                        trade.stopLossOrder.price = currentCandle.bid.c - adjustmentMargin;
                    }

                }
            }
            // Sell trade
            else
            {
                // Stop loss condition
                if (currentCandle.ask.c >= trade.stopLossOrder.price)
                {
                    trade.realizedPL = (trade.price - currentCandle.ask.c) * trade.initialUnits;
                    trade.closeTime = time;
                    _openTrades.Remove(trade);
                    _closedTrades.Add(trade);
                }

                // Take profit condition
                if (currentCandle.ask.c <= trade.takeProfitOrder.price)
                {
                    trade.realizedPL = (trade.price - currentCandle.ask.c) * trade.initialUnits;
                    trade.closeTime = time;
                    _openTrades.Remove(trade);
                    _closedTrades.Add(trade);
                }

                // Adjust SL
                if (currentCandle.ask.c < trade.price)
                {
                    // Set a positive SL if is yet negative
                    if (trade.stopLossOrder.price > trade.price)
                    {
                        trade.stopLossOrder.price = currentCandle.ask.c;
                    }

                    var adjustmentMargin = Math.Abs(trade.takeProfitOrder.price - trade.price) * (decimal)0.1;

                    // Grow SL to ensure more profit
                    if (trade.stopLossOrder.price > currentCandle.ask.c + adjustmentMargin)
                    {
                        trade.stopLossOrder.price = currentCandle.ask.c + adjustmentMargin;
                    }
                }
            }

        }
    }
}
